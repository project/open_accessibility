Description
-----------
This module helps user to access the basic accessibility for the website. It was completely open source code taken from the Jossef Harush Kadouri's Open Accessibility codes. Free accessibility tools menu for website maintainers powered by jQuery.

Installation
------------
1. Install module using composer or normal way
2. Configure the settings for Open Accessibility
  /admin/config/user-interface/open-accessibility
3. Provide permission for the access configuration page
4. Place the Open Accessibility Block into your region.

Reference:
---------
[1] https://github.com/jossef/open-accessibility